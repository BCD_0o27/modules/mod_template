/** 
 * @file        mod_template.hpp
 * @author      Florian Schütz (fschuetz@ieee.org)
 * @brief       Template file for BalcCon Cyberdeck modules.
 * @version     1.0
 * @date        12.09.2023
 * @copyright   Copyright (c) 2022, Florian Schuetz, released under MIT license
 *
 * This file serves as a template to programm a module to be run on the BalcCon
 * cyberdeck. Consult the developer documentation and examples for more 
 * information.
 */

#pragma once
#ifndef BCD_MOD_TEMPLATE_HPP
#define BCD_MOD_TEMPLATE_HPP

#include <stdlib.h>
#include "esp_log.h"
#include "bcd_system.hpp"
#include "st7735_bcd.hpp"
#include "gfx.hpp"
#include "ch405labs_esp_led.hpp"
#include "ch405labs_esp_debug.h"
#include "ch405labs_esp_wifi.hpp"
#include "ch405labs_esp_controller.hpp"

////////////////////////////////////////////////////////////////////////////////
// Menuconfig options
////////////////////////////////////////////////////////////////////////////////
#define TAG_MOD_TEMPLATE                   CONFIG_TAG_MOD_TEMPLATE

#if CONFIG_MOD_TEMPLATE_LOG_LEVEL == 0
#define MOD_TEMPLATE_LOG_LEVEL esp_log_level_t::ESP_LOG_NONE
#elif CONFIG_MOD_TEMPLATE_LOG_LEVEL == 1
#define MOD_TEMPLATE_LOG_LEVEL esp_log_level_t::ESP_LOG_ERROR
#elif CONFIG_MOD_TEMPLATE_LOG_LEVEL == 2
#define MOD_TEMPLATE_LOG_LEVEL esp_log_level_t::ESP_LOG_WARN
#elif CONFIG_MOD_TEMPLATE_LOG_LEVEL == 3
#define MOD_TEMPLATE_LOG_LEVEL esp_log_level_t::ESP_LOG_INFO
#elif CONFIG_MOD_TEMPLATE_LOG_LEVEL == 4
#define MOD_TEMPLATE_LOG_LEVEL esp_log_level_t::ESP_LOG_DEBUG
#elif CONFIG_MOD_TEMPLATE_LOG_LEVEL == 5
#define MOD_TEMPLATE_LOG_LEVEL esp_log_level_t::ESP_LOG_VERBOSE
#endif //CONFIG_MOD_TEMPLATE_LOG_LEVEL

////////////////////////////////////////////////////////////////////////////////
// Error handling
////////////////////////////////////////////////////////////////////////////////
typedef BaseType_t mod_template_err_t;

#define MOD_TEMPLATE_FAIL                                           -1          /**< Generic failure */
#define MOD_TEMPLATE_OK                                             0x000       /**< All good */


////////////////////////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////////////////////////
// Put any macro here.

////////////////////////////////////////////////////////////////////////////////
// Namespaces
////////////////////////////////////////////////////////////////////////////////
// If you "use" namespaces, put them here.

////////////////////////////////////////////////////////////////////////////////
// Various Types
////////////////////////////////////////////////////////////////////////////////
// Put type definitions that don't fit above here.

// Any module should reside in its own namespace using the scheme
// bcd_mod_<name of your module>. Please adapt to the name of your module.
namespace bcd_mod_template {

    ////////////////////////////////////////////////////////////////////////////
    // Main function
    ////////////////////////////////////////////////////////////////////////////
    // While not mandatory, it is customary to call the entry point into your
    // module `module_main()`. It must return an integer (0 on success) and 
    // has a void * parameter. The void pointer can point to more complex 
    // structures, which allow you to pass multiple parameters. 
    int module_main(void *param);

    ////////////////////////////////////////////////////////////////////////////
    // Function prototypes, classes, ...
    ////////////////////////////////////////////////////////////////////////////
    // Put your function prototypes or class definitions etc... here.

} // namespace bcd_mod_template
#endif // BCD_MOD_TEMPLATE_HPP
