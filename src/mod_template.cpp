/** 
 * @file        mod_template.cpp
 * @author      Florian Schütz (fschuetz@ieee.org)
 * @brief       Template file for BalcCon Cyberdeck modules.
 * @version     1.1
 * @date        12.09.2023
 * @copyright   Copyright (c) 2022, Florian Schuetz, released under MIT license
 *
 * This file serves as a template to programm a module to be run on the BalcCon
 * cyberdeck. Consult the developer documentation and examples for more 
 * information.
 */
#include "../include/mod_template.hpp"

namespace bcd_mod_template {
    ////////////////////////////////////////////////////////////////////////////
    // Main function
    ////////////////////////////////////////////////////////////////////////////
    // While not mandatory, it is customary to call the entry point into your
    // module `module_main()`. It must return an integer (0 on success) and 
    // has a void * parameter. The void pointer can point to more complex 
    // structures, which allow you to pass multiple parameters. 
    int module_main(void *param) {

#ifdef CONFIG_DEBUG_STACK
        UBaseType_t uxHighWaterMark;

        /* Inspect our own high water mark on entering the task. */
        uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        ESP_LOGD(CONFIG_TAG_STACK, "mod_template::module_main(): High watermark"
            " for stack at start is: %d", uxHighWaterMark);
#endif //CONFIG_DEBUG_STACK

#ifdef CONFIG_DEBUG_HEAP
        multi_heap_info_t heap_info; 
        heap_caps_get_info(&heap_info, MALLOC_CAP_DEFAULT);
        ESP_LOGD(CONFIG_TAG_HEAP,   "mod_template::module_main(): Heap state at"
                                    " start: \n"
                                    "            Free blocks:           %d\n"
                                    "            Allocated blocks:      %d\n"
                                    "            Total blocks:          %d\n"
                                    "            Largest free block:    %d\n"
                                    "            Total free bystes:     %d\n"
                                    "            Total allocated bytes: %d\n"
                                    "            Minimum free bytes:    %d\n"
                                    , heap_info.free_blocks 
                                    , heap_info.allocated_blocks
                                    , heap_info.total_blocks
                                    , heap_info.largest_free_block
                                    , heap_info.total_free_bytes
                                    , heap_info.total_allocated_bytes
                                    , heap_info.minimum_free_bytes);
#endif //CONFIG_DEBUG_HEAP

	// <--- PLACE CODE BELOW HERE -->

	// <--- PLACE CODE ABOVE HERE -->
	
 #ifdef CONFIG_DEBUG_STACK
        /* Inspect our own high water mark before exiting the task. */
        uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        ESP_LOGD(CONFIG_TAG_STACK, "mod_template::module_main(): High watermark"
            " for stack at end is: %d", uxHighWaterMark);
 #endif //CONFIG_DEBUG_STACK

	    return 0;
    }
} //namespace bcd_mod_template

